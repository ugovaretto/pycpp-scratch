import numpy as np
import npytest as t
a = np.arange(5)
t.fun(a)
a = np.arange(5,dtype=np.float32)
t.fun(a)
a = np.array([1. + 1.j, 2. + 2.j, 3. + 3.j], dtype=np.complex)
t.fun(a)
a = np.array(['123', '456', '234', 'ddd'], dtype='|S3')
t.fun(a)