cmake_minimum_required(VERSION 3.3)
project(pycpp_scratch)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(NUMPYINCLUDES /Library/Frameworks/Python.framework/Versions/3.5/lib/python3.5/site-packages/numpy-1.10.2-py3.5-macosx-10.6-intel.egg/numpy/core/include)
set(NUMPYLIBS multiarray.cpython-35m-darwin)
set(NUMPYLIBDIR /Library/Frameworks/Python.framework/Versions/3.5/lib/python3.5/site-packages/numpy-1.10.2-py3.5-macosx-10.6-intel.egg/numpy/core)

include_directories(/Library/Frameworks/Python.framework/Versions/3.5/include/python3.5m)
link_directories(/Library/Frameworks/Python.framework/Versions/3.5/lib/python3.5/config-3.5m)

add_library(spam SHARED spammodule.cpp)
add_library(npytest SHARED npytestmodule.cpp)

target_include_directories(npytest PUBLIC ${NUMPYINCLUDES})
target_link_libraries(spam python3.5m)
target_link_libraries(npytest python3.5m)

