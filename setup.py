#SPAM Module build
from distutils.core import setup, Extension

module1 = Extension('spam',
                    sources = [ 'spammodule.cpp' ],
                    libraries = [],
                    language = "c++",
                    extra_compile_args=["-std=c++11"])

setup(name ='spam',
      version = '1.0',
      description = 'SPAM!',
      ext_modules = [module1])