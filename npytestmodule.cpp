//
// Minimal example of how to create a Python extension
// If not using distutils simply build the extension as a
// standard shared library then update the PYTHONPATH var accordingly
// and on Mac OS make sure the name is <modulename>.so

// WARNING: 'i' type is long int (64 bit) on 64 bit platforms!!!

#include <cassert>
#include <iostream>
#include <string>
#include <Python.h>
#include <numpy/arrayobject.h>

using namespace std;

static PyObject*
npytest_fun(PyObject* self, PyObject* args) {
    assert(args);

    PyObject* rep = PyObject_Repr(args);
    assert(rep);
    if(PyUnicode_Check(rep)) {
        PyObject* temp_bytes = PyUnicode_AsEncodedString(rep, "ASCII", "strict");
        cout << PyBytes_AS_STRING(temp_bytes) << endl;
    };

    PyArrayObject* array = nullptr;
    //the object in converted but refcount it is not incremented
    //if returning a (modified version) copy of the same object do
    //increment the ref count first
    if(!PyArg_ParseTuple(args, "O!",
                         &PyArray_Type, &array)) {

        return nullptr;
    }
    assert(array);
    cout << "DIM:  " << PyArray_NDIM(array) << endl
         << "Type: " << PyArray_TYPE(array) << endl;
    PyArray_Descr* descr = PyArray_DESCR(array);
    assert(descr);
    cout << "Kind: " << descr->kind << endl;
    cout << "El.size: " << descr->elsize << " bytes" << endl;
    cout << "Subarray ?: " << boolalpha << (descr->subarray != nullptr) << endl;
    const npy_intp* shape = PyArray_SHAPE(array);
    for(int d = 0; d != PyArray_NDIM(array); ++d) {
        cout << shape[d] << ' ';
    }
    cout << endl;
    assert(PyArray_NDIM(array) == 1);
    assert(shape[0] > 2);
    cout << "Array[2]: ";
    switch(PyArray_TYPE(array)) {
        case NPY_LONG: {
            //'i' == long int with python >= 3.5 on 64 bit platforms!!!
            const long int *pInt =
                    reinterpret_cast< long int * >(PyArray_DATA(array));
            cout << pInt[2] << " long";
            break;
        }
        case NPY_INT: {
            const int *pInt =
                    reinterpret_cast< int * >(PyArray_DATA(array));
            cout << pInt[2] << " int";
            break;
        }
        case NPY_DOUBLE: {
            const double *pDouble =
                    reinterpret_cast< double * >(PyArray_DATA(array));
            cout << pDouble[2] << " double";
            break;
        }
        case NPY_FLOAT: {
            const float *pFloat =
                    reinterpret_cast< float * >(PyArray_DATA(array));
            cout << pFloat[2] << " float";
            break;
        }
        //complex 128 = 2 x 64bit doubles
        case NPY_COMPLEX128: {
            const double *pDouble =
                    reinterpret_cast< double * >(PyArray_DATA(array));
            cout << '(' << pDouble[2] << ", " << pDouble[2 + 1] << ") complex";
            break;
        }
        case NPY_UNICODE: {
            const wchar_t *pChar =
                    reinterpret_cast< wchar_t * >(PyArray_DATA(array));
            cout << pChar[2] << " unicode";
            break;
        }
        case NPY_STRING: {
            const char *pChar =
                    reinterpret_cast< char * >(PyArray_DATA(array));
            const string s(&pChar[descr->elsize * 2],
                           &pChar[descr->elsize * 2] + descr->elsize);
            cout << s << " string";
            break;
        }
        default:
            break;
    }
    cout << "\n=========================\n" << endl;

    Py_INCREF(Py_None);
    return static_cast< PyObject* >(Py_None);
}

static PyMethodDef npyTestMethods[] = {
        {"fun", npytest_fun, METH_VARARGS, "Call a function"},
        {nullptr, nullptr, 0, nullptr}
};

static struct PyModuleDef npyTestModule = {
        PyModuleDef_HEAD_INIT,
        "numpytest",
        "NUMPY",
        -1,
        npyTestMethods
};

PyMODINIT_FUNC
PyInit_npytest() {
    PyObject* module = PyModule_Create(&npyTestModule);
    if(nullptr == module) return nullptr;
    import_array(); //!!! segfaults at PyArg_ParseTuple if not included!!!
    return module;
}
