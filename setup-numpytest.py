#numpytest Module build
from distutils.core import setup, Extension
import numpy as np

module1 = Extension('npytest',
                    sources = [ 'npytestmodule.cpp' ],
                    libraries = [],
                    language = "c++",
                    extra_compile_args=["-std=c++11"],
                    include_dirs=[np.get_include()])

setup(name ='npytest',
      version = '1.0',
      description = 'NUMPY TEST',
      ext_modules = [module1])